using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{

    public GameObject[] dieCollection;
    public GameObject[] diePosition;
    public GameObject diceContainer;

    private DieScript dieScript;
    private DieBox dieBox;

    // Start is called before the first frame update
    void Start()
    {
        // dieCollection = new GameObject[30];
        // diePosition = new GameObject[10];

        SpawnDice();
    }

    // Update is called once per frame
    void Update()
    {

        if (Input.GetMouseButtonDown(0))
        {
            Vector3 mousePos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            Vector2 mousePos2D = new Vector2(mousePos.x, mousePos.y);

            RaycastHit2D hit = Physics2D.Raycast(mousePos2D, Vector2.zero);
            if (hit.collider != null)
            {
                Debug.Log("Something was clicked!");
                Debug.Log(hit.collider.name);
                if(hit.collider.tag == "Dice")
                {
                    dieScript = hit.transform.gameObject.GetComponent<DieScript>();
                    Debug.Log("Dice was clicked!");
                    dieBox = diceContainer.GetComponent<DieBox>();
                    if (dieScript.currentZone.name == "Play_1" || dieScript.currentZone.name == "Play_2" || dieScript.currentZone.name == "Play_3" || dieScript.currentZone.name == "Play_4" || dieScript.currentZone.name == "Play_5" || dieScript.currentZone.name == "DiceContainer")
                    {
                        Debug.Log("Dice was in play area!");
                        if (dieScript.currentZone.name == "DiceContainer")
                        {
                            Debug.Log("Clearing Dice Box, subtract swap count by one");
                            Destroy(hit.transform.gameObject);
                        }
                        else if (dieBox.collisionCount == 0)
                        {
                            Debug.Log("Die Box is empty, any move is valid");
                            Debug.Log("Move Dice to Box");
                            dieScript.MoveDice();
                        }
                        else if (dieScript.dieNumber == 2 && dieBox.dieNumber == 1)
                        {
                            Debug.Log("Die number is next in sequence");
                            Debug.Log("Move Dice to Box");
                            dieScript.MoveDice();
                        }
                        else if (dieScript.dieNumber == 3 && dieBox.dieNumber == 2)
                        {
                            Debug.Log("Die number is next in sequence");
                            Debug.Log("Move Dice to Box");
                            dieScript.MoveDice();
                        }
                        else if (dieScript.dieNumber == 4 && dieBox.dieNumber == 3)
                        {
                            Debug.Log("Die number is next in sequence");
                            Debug.Log("Move Dice to Box");
                            dieScript.MoveDice();
                        }
                        else if (dieScript.dieNumber == 5 && dieBox.dieNumber == 4)
                        {
                            Debug.Log("Die number is next in sequence");
                            Debug.Log("Move Dice to Box");
                            dieScript.MoveDice();
                        }
                        else if (dieScript.dieNumber == 6 && dieBox.dieNumber == 5)
                        {
                            Debug.Log("Die number is next in sequence");
                            Debug.Log("Move Dice to Box");
                            dieScript.MoveDice();
                        }
                        else if (dieScript.dieNumber == 1 && dieBox.dieNumber == 6)
                        {
                            Debug.Log("Die number is next in sequence");
                            Debug.Log("Move Dice to Box");
                            dieScript.MoveDice();
                        }
                        else if ((int)dieScript.DieColor == (int)dieBox.DieColor)
                        {
                            Debug.Log("Dice color enum indexes match");
                            Debug.Log("Move Dice to Box");
                            dieScript.MoveDice();
                        }
                        else
                        {
                            Debug.Log("Invalid Move");
                            Debug.Log("Shake Dice");
                        }
                    }
                }
            }
        }
    }

    void SpawnDice()
    {
        for(int i = 0; i < diePosition.Length; i++)
        {
            Instantiate (dieCollection[Random.Range(0, dieCollection.Length)], diePosition[i].transform.position, diePosition[i].transform.rotation);
        }
    }
}
