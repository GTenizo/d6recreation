using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DieScript : MonoBehaviour
{
    public GameObject currentZone;
    public GameObject diceContainer;

    public int dieNumber;

    public enum DieColorList { aquamarine, fuchsia, khaki, peru, rosybrown }

    public DieColorList DieColor;

    public float moveSpeed = 50f;
    private Transform target;
    private bool moveToBox = false;
    

    // Start is called before the first frame update
    void Start()
    {
        diceContainer = GameObject.Find("DiceContainer");
    }

    // Update is called once per frame
    void Update()
    {
        if (moveToBox)
        {
            float step = moveSpeed * Time.deltaTime;
            transform.position = Vector3.MoveTowards(transform.position, target.position, step);
        }
    }

    public void MoveDice()
    {

        target = diceContainer.transform;
        
        moveToBox = true;
        //transform.position = Vector3.MoveTowards(transform.position, target.position, step);
        if(currentZone.name == "DiceContainer")
        {
            Destroy(gameObject);
        }
    }

}
