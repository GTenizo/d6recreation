using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayRow : MonoBehaviour
{

    private DieScript dieScript;

    public int dieNumber;
    public enum DieColorList { aquamarine, fuchsia, khaki, peru, rosybrown }

    public DieColorList DieColor;

    
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.CompareTag("Dice"))
        {

            dieScript = other.GetComponent<DieScript>();
            dieNumber = dieScript.dieNumber;
            dieScript.currentZone = gameObject;
            DieColor = (DieColorList)dieScript.DieColor;
        }
    }
}
