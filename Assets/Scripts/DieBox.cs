using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DieBox : MonoBehaviour
{
    private DieScript dieScript;

    public int collisionCount = 0;

    public bool BoxIsEmpty
    {
        get { return collisionCount == 0; }
    }

    public int dieNumber;

    public enum DieColorList { aquamarine, fuchsia, khaki, peru, rosybrown }

    public DieColorList DieColor;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {

    }

    void OnTriggerEnter2D(Collider2D other)
    {
        Debug.Log("Dice Box Collision");
        if (other.gameObject.CompareTag("Dice"))
        {
            Debug.Log("Dice Box Collision");
            collisionCount++;
            dieScript = other.GetComponent<DieScript>();
            dieNumber = dieScript.dieNumber;
            dieScript.currentZone = gameObject;
            DieColor = (DieColorList)dieScript.DieColor;
        }
        
    }

    void OnTriggerExit2D(Collider2D other)
    {
        if (other.gameObject.CompareTag("Dice"))
        {
            collisionCount--;
        }
    }
}
